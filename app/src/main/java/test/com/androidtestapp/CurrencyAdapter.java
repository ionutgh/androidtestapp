package test.com.androidtestapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {

    private static final int MAX_INT_DIGITS = 20;
    private static final int MAX_DECIMAL = 2;
    private OnCurrencyClickedListener onCurrencyClickedListener;
    private List<String> currencies;

    DecimalFormat formater = new DecimalFormat("#.##");

    TwoDigitsTextWatcher amountTextWatcher = new TwoDigitsTextWatcher();

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CurrencyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, final int position, @NonNull List<Object> payloads) {
        if (position == 0) {
            amountTextWatcher.setSource(holder.amount);
            holder.amount.addTextChangedListener(amountTextWatcher);
            holder.overlay.setVisibility(View.GONE);
        } else {
            holder.amount.removeTextChangedListener(amountTextWatcher);
            holder.overlay.setVisibility(View.VISIBLE);
        }
        holder.overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCurrencyClickedListener != null) {
                    onCurrencyClickedListener.onCurrencyClicked(position);
                }
            }
        });
        if (payloads != null && !payloads.isEmpty()) {
            for (Object payload : payloads) {
                if ("rates".equals(String.valueOf(payload))) {
                    holder.amount.setText(formater.format(ExchangeRateData.getInstance().getAmount() * ExchangeRateData.getInstance().getRates().get(currencies.get(position))));
                }
                if ("flags".equals(String.valueOf(payload))) {
                    int resourceId = holder.itemView.getContext().getResources().getIdentifier("flag_" + currencies.get(position).toLowerCase(), "drawable", holder.itemView.getContext().getPackageName());
                    if (resourceId >= 0) {
                        holder.image.setImageResource(resourceId);
                    } else {
                        holder.image.setImageResource(R.drawable.flag_placeholder);
                    }
                }
            }

        } else {
            onBindViewHolder(holder, position);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, final int position) {
        holder.codeView.setText(currencies.get(position));
        Currency currency = Currency.getInstance(currencies.get(position));
        if (currency != null) {
            holder.nameView.setText(currency.getDisplayName(Locale.getDefault()));
        } else {
            holder.nameView.setText("");
        }

        int resourceId = holder.itemView.getContext().getResources().getIdentifier("flag_" + currencies.get(position).toLowerCase(), "drawable", holder.itemView.getContext().getPackageName());
        if (resourceId >= 0) {
            holder.image.setImageResource(resourceId);
        } else {
            holder.image.setImageResource(R.drawable.flag_placeholder);
        }
        if (position == 0) {
            holder.amount.requestFocus();
            holder.amount.setText(formater.format(ExchangeRateData.getInstance().getAmount()));
        } else {
            if (ExchangeRateData.getInstance().getRates() != null && ExchangeRateData.getInstance().getRates().containsKey(currencies.get(position))) {
                holder.amount.setText(formater.format(ExchangeRateData.getInstance().getAmount() * ExchangeRateData.getInstance().getRates().get(currencies.get(position))));
            }
        }
    }


    @Override
    public int getItemCount() {
        if (currencies == null) {
            return 0;
        }
        return currencies.size();
    }

    public void updateRates() {
        for (int i = 1; i < getItemCount(); i++) {
            notifyItemChanged(i, "rates");
        }
    }

    public void updateCurrencyFlags() {
        for (int i = 1; i < getItemCount(); i++) {
            notifyItemChanged(i, "flags");
        }
    }


    public void setOnCurrencyClickedListener(OnCurrencyClickedListener onCurrencyClickedListener) {
        this.onCurrencyClickedListener = onCurrencyClickedListener;
    }

    public void setCurrencies() {
        if (this.currencies == null) {
            this.currencies = new ArrayList<>();
            this.currencies.add(ExchangeRateData.getInstance().getBaseCurrency());
            this.currencies.addAll(ExchangeRateData.getInstance().getCurrencies());
            notifyDataSetChanged();
        } else {
            if (!this.currencies.get(0).equals(ExchangeRateData.getInstance().getBaseCurrency())) {
                this.currencies.remove(ExchangeRateData.getInstance().getBaseCurrency());
                this.currencies.add(0, ExchangeRateData.getInstance().getBaseCurrency());
            }
        }

    }

    public List<String> getCurrencies() {
        return currencies;
    }

    class CurrencyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView codeView;
        TextView nameView;
        EditText amount;
        View overlay;

        public CurrencyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.currency_image);
            codeView = itemView.findViewById(R.id.currency_code);
            nameView = itemView.findViewById(R.id.currency_name);
            amount = itemView.findViewById(R.id.amount);
            overlay = itemView.findViewById(R.id.overlay);
        }
    }

    public interface OnCurrencyClickedListener {
        public void onCurrencyClicked(int position);
    }

    public class TwoDigitsTextWatcher implements TextWatcher {

        private EditText source;

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (source == null) {
                return;
            }
            String str = source.getText().toString();
            if (str.isEmpty()) return;
            String str2 = trimString(str, MAX_INT_DIGITS, MAX_DECIMAL);

            if (!str2.equals(str)) {
                source.setText(str2);
                int pos = source.getText().length();
                source.setSelection(pos);
            }
            try {
                Float newAmount = Float.valueOf(source.getText().toString());
                if (newAmount != ExchangeRateData.getInstance().getAmount()) {
                    ExchangeRateData.getInstance().setAmount(newAmount);
                    updateRates();
                }
            } catch (Exception e) {

            }
        }

        public void setSource(EditText source) {
            this.source = source;
        }

        public String trimString(String str, int maxInt, int maxDecimal) {

            int i = 0;
            while (i < str.length() && str.charAt(i) == '0') {
                i++;
            }
            if (i < str.length() && (i == str.length() - 1 || str.charAt(i) != '.')) {
                str = str.substring(i);
            }

            if (str.charAt(0) == '.') {
                str = "0" + str;
            }
            int max = str.length();

            String rFinal = "";
            boolean after = false;
            i = 0;
            int up = 0, decimal = 0;
            char t;
            while (i < max) {
                t = str.charAt(i);
                if (t != '.' && !after) {
                    up++;
                    if (up > maxInt) return rFinal;
                } else if (t == '.') {
                    after = true;
                } else {
                    decimal++;
                    if (decimal > maxDecimal) {
                        return rFinal;
                    }
                }
                rFinal = rFinal + t;
                i++;
            }
            return rFinal;
        }
    }
}
