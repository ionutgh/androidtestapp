package test.com.androidtestapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.ArrayList;

public class CurrencyActivity extends AppCompatActivity {

    BroadcastReceiver exchangeRatesBroadcastReceiver;
    Toolbar toolbar;
    RecyclerView ratesRecyclerView;
    boolean listUpdatePending = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
        exchangeRatesBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(ratesRecyclerView != null && ratesRecyclerView.getAdapter() != null) {
                    if(ratesRecyclerView.getScrollState() == RecyclerView.SCROLL_STATE_IDLE) {
                        ((CurrencyAdapter)ratesRecyclerView.getAdapter()).setCurrencies();
                        ((CurrencyAdapter)ratesRecyclerView.getAdapter()).updateRates();
                    } else {
                        listUpdatePending = true;
                    }
                }
            }
        };
        inflate();
        setupUI();
    }

    private void inflate() {
        ratesRecyclerView = findViewById(R.id.recycler);
        toolbar = findViewById(R.id.toolbar);
    }

    private void setupUI() {
        setSupportActionBar(toolbar);
        ratesRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        final CurrencyAdapter adapter = new CurrencyAdapter();
        adapter.setOnCurrencyClickedListener(new CurrencyAdapter.OnCurrencyClickedListener() {
            @Override
            public void onCurrencyClicked(int position) {

                if(position > 0 && !ExchangeRateData.getInstance().isWaitingForRates()) {
                    ExchangeRateData.getInstance().setBaseCurrency(adapter.getCurrencies().get(position));
                    adapter.notifyItemMoved(position, 0);
                    ratesRecyclerView.scrollToPosition(0);
                    ((CurrencyAdapter)ratesRecyclerView.getAdapter()).setCurrencies();
                    ((CurrencyAdapter)ratesRecyclerView.getAdapter()).updateCurrencyFlags();
                    adapter.notifyItemChanged(0);

                }
            }
        });
        ratesRecyclerView.setAdapter(adapter);

        ratesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE && listUpdatePending) {
                    listUpdatePending = false;
                    ((CurrencyAdapter)ratesRecyclerView.getAdapter()).setCurrencies();
                    ((CurrencyAdapter)ratesRecyclerView.getAdapter()).updateRates();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(getApplicationContext(), ExchangeRateService.class));
        LocalBroadcastManager.getInstance(this).registerReceiver((exchangeRatesBroadcastReceiver), new IntentFilter(ExchangeRateService.RATES_ACTION));
    }

    @Override
    protected void onStop() {
        stopService(new Intent(getApplicationContext(), ExchangeRateService.class));
        LocalBroadcastManager.getInstance(this).unregisterReceiver(exchangeRatesBroadcastReceiver);
        super.onStop();
    }

}
