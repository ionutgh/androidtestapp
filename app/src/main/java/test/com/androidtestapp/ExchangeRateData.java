package test.com.androidtestapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ExchangeRateData {
    private static final ExchangeRateData ourInstance = new ExchangeRateData();

    private String baseCurrency = "EUR";
    private Map<String, Double> rates;

    //on each update calculate
    private Map<String, Double> tempValues;
    private List<String> currencies;
    private double amount = 0;
    private boolean waitingForRates = false;

    public static ExchangeRateData getInstance() {
        return ourInstance;
    }

    private ExchangeRateData() {
    }

    public void update(ExchangeRatesResponse response) {
        baseCurrency = response.getBase();
        rates = response.getRates();
        currencies = new ArrayList<>(rates.keySet());
        waitingForRates = false;
    }



    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        rates.put(this.baseCurrency, 1 / rates.get(baseCurrency));
        this.baseCurrency = baseCurrency;
        setAmount(amount * rates.get(baseCurrency));
        waitingForRates = true;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public boolean isWaitingForRates() {
        return waitingForRates;
    }

}
