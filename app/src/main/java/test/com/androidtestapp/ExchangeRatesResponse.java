package test.com.androidtestapp;

import java.io.Serializable;
import java.util.Map;

public class ExchangeRatesResponse implements Serializable {
    private String base;
    private String date;
    private Map<String, Double> rates;

    public Map<String, Double> getRates() {
        return rates;
    }

    public String getBase() {
        return base;
    }
}
