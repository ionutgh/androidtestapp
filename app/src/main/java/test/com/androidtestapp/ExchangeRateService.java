package test.com.androidtestapp;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class ExchangeRateService extends Service {

    private final long DATA_FETCH_INTERVAL = 1000;
    public static final String EXCHANGE_RATE_URL = "https://revolut.duckdns.org/latest?base=%s";

    public static final String RATES_ACTION = "test.com.androidtestapp.EXCHANGE_RATES_UPDATED";

    public static final String RATES_DATA = ".exchangeRates";



    private Handler handler;
    private LocalBroadcastManager broadcastManager;

    private Runnable fetchRunnable = new Runnable() {
        @Override
        public void run() {
            StringRequest request = new StringRequest(Request.Method.GET, String.format(EXCHANGE_RATE_URL, ExchangeRateData.getInstance().getBaseCurrency()),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //Log.d("EXCHANGE_RATE", "RECEIVED: " + response);
                            Type type = new TypeToken<ExchangeRatesResponse>(){}.getType();
                            try {
                                ExchangeRatesResponse exchangeRatesResponse = new Gson().fromJson(response, type);
                                broadcastResult(exchangeRatesResponse);
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                            handler.postDelayed(fetchRunnable, DATA_FETCH_INTERVAL);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            handler.postDelayed(fetchRunnable, DATA_FETCH_INTERVAL);
                        }
                    });
            if(getApplication() instanceof CurrencyApplication) {
                request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                ((CurrencyApplication) getApplication()).getRequestQueue().add(request);
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        broadcastManager = LocalBroadcastManager.getInstance(this);
        handler.post(fetchRunnable);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if(handler != null) {
            handler.removeCallbacks(fetchRunnable);
        }
        stopSelf();
        super.onDestroy();
    }

    private void broadcastResult(ExchangeRatesResponse exchangeRatesResponse) {
        Intent intent = new Intent(RATES_ACTION);
        if(exchangeRatesResponse != null && exchangeRatesResponse.getBase() != null && exchangeRatesResponse.getBase().equals(ExchangeRateData.getInstance().getBaseCurrency())) {
            ExchangeRateData.getInstance().update(exchangeRatesResponse);
            broadcastManager.sendBroadcast(intent);
        }
    }
}
