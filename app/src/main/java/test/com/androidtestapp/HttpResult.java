package test.com.androidtestapp;

class HttpResult {
    private int responseCode;
    private String response;

    HttpResult(int responseCode, String response) {
        this.responseCode = responseCode;
        this.response = response;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponse() {
        return response;
    }
}
